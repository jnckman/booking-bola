<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware'=>['web','\crocodicstudio\crudbooster\middlewares\CBBackend']],function(){
	Route::post('admin/booking/ajaxpostbooking',['as'=>'booking.ajaxpostbooking','uses'=>'AdminBookingsController@ajaxpostbooking']);
	// Route::get('admin/booking/ajaxpostbooking',['as'=>'booking.ajaxpostbooking','uses'=>'AdminBookingsController@ajaxpostbooking']);
	Route::get('admin/dashboard/{id}',['as'=>'booking.dashboardlapangan','uses'=>'AdminBookingsController@gethomebylapangan']);	
	Route::post('admin/bookings?q=belum', ['as'=>'bookings.belumlunas' ,'uses'=>'AdminBookingsController@index']);
	Route::post('admin/bookings?mendatang=1', ['as'=>'bookings.mendatang' ,'uses'=>'AdminBookingsController@index']);
	Route::post('admin/bookings?q=lunas', ['as'=>'bookings.lunas' ,'uses'=>'AdminBookingsController@index']);

	Route::post('admin/booking/ajaxeditbooking',['as'=>'booking.ajaxeditbooking','uses'=>'AdminBookingsController@ajaxeditbooking']);
	// Route::get('admin/booking/ajaxeditbooking',['as'=>'booking.ajaxeditbooking','uses'=>'AdminBookingsController@ajaxeditbooking']);

	Route::post('admin/booking/postbayar',['as'=>'booking.postbayar','uses'=>'AdminBookingsController@postbayar']);


	Route::get('admin/bookings/ajaxgetpaket',['as'=>'booking.ajaxgetpaket','uses'=>'AdminBookingsController@ajaxgetpaket']);
	Route::get('admin/bookings/ajaxgetbookings',['as'=>'booking.ajaxgetbookings','uses'=>'AdminBookingsController@ajaxgetbookings']);

    Route::get('admin/laporan/dp',['as'=>'laporan.dp','uses'=>'AdminBookingsController@laporandp']);
    Route::get('admin/laporan/get_table_laporandp',['as'=>'get_table_laporandp','uses'=>'AdminBookingsController@get_table_laporandp']);

    Route::get('admin/laporan/pelunasan',['as'=>'laporan.pelunasan','uses'=>'AdminBookingsController@pelunasan']);
    Route::get('admin/laporan/get_table_laporanpelunasan',['as'=>'get_table_laporanpelunasan','uses'=>'AdminBookingsController@get_table_laporanpelunasan']);

    Route::get('admin/laporan/keuangan',['as'=>'laporan.keuangan','uses'=>'AdminBookingsController@keuangan']);
    Route::get('admin/laporan/get_table_laporankeuangan',['as'=>'get_table_laporankeuangan','uses'=>'AdminBookingsController@get_table_laporankeuangan']);

    Route::get('admin/home/ajaxgraphpendapatan',['as'=>'home.ajaxgraphpendapatan','uses'=>'AdminBookingsController@ajaxgraphpendapatan']);
});