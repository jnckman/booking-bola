<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAwal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->softDeletes();
            $table->timestamps();
            $table->date('tanggal')->nullable();
            $table->integer('durasi')->default(0)->unsigned();
            $table->time('jam_mulai');
            $table->time('jam_akhir');
            $table->text('catatan');
            $table->integer('lapangans_id')->default(0)->unsigned();
            $table->string('pembooking',255);
            $table->integer('operator')->default(0)->unsigned();
            $table->integer('dp')->default(0)->unsigned();
            $table->integer('lunas')->default(0)->unsigned();
            $table->date('tanggal_lunas');
            $table->integer('total')->default(0)->unsigned();
            $table->time('jam_booking');
            $table->integer('pakets_id')->default(0)->unsigned();
        });

        Schema::create('lapangans', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('nama');
            $table->string('kode',255);
        });
        Schema::create('pakets', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('nama');
            $table->integer('jam')->default(0)->unsigned();
            $table->integer('harga')->default(0)->unsigned();
        });

        Schema::create('pengeluarans', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->date('tanggal');
            $table->integer('operator')->default(0)->unsigned();
            $table->integer('nominal')->default(0)->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bookings');
        Schema::drop('lapangans');
        Schema::drop('pakets');
        Schema::drop('pengeluarans');
    }
}
