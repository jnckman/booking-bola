<?php

use Illuminate\Database\Seeder;

class CmsMenusTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('cms_menus')->delete();
        
        \DB::table('cms_menus')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'List Booking',
                'type' => 'Route',
                'path' => 'AdminBookingsControllerGetIndex',
                'color' => 'normal',
                'icon' => 'fa fa-bookmark',
                'parent_id' => 6,
                'is_active' => 1,
                'is_dashboard' => 0,
                'id_cms_privileges' => 1,
                'sorting' => 1,
                'created_at' => '2018-01-18 18:21:10',
                'updated_at' => '2018-01-22 10:20:02',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Lapangans',
                'type' => 'Route',
                'path' => 'AdminLapangansControllerGetIndex',
                'color' => NULL,
                'icon' => 'fa fa-th-list',
                'parent_id' => 0,
                'is_active' => 1,
                'is_dashboard' => 0,
                'id_cms_privileges' => 1,
                'sorting' => 5,
                'created_at' => '2018-01-18 18:24:56',
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Pakets',
                'type' => 'Route',
                'path' => 'AdminPaketsControllerGetIndex',
                'color' => NULL,
                'icon' => 'fa fa-book',
                'parent_id' => 0,
                'is_active' => 1,
                'is_dashboard' => 0,
                'id_cms_privileges' => 1,
                'sorting' => 6,
                'created_at' => '2018-01-18 18:32:53',
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Pengeluarans',
                'type' => 'Route',
                'path' => 'AdminPengeluaransControllerGetIndex',
                'color' => NULL,
                'icon' => 'fa fa-money',
                'parent_id' => 0,
                'is_active' => 1,
                'is_dashboard' => 0,
                'id_cms_privileges' => 1,
                'sorting' => 7,
                'created_at' => '2018-01-18 18:33:21',
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Home',
                'type' => 'Controller & Method',
                'path' => 'AdminBookingsController@getHome',
                'color' => 'normal',
                'icon' => 'fa fa-bookmark',
                'parent_id' => 0,
                'is_active' => 1,
                'is_dashboard' => 1,
                'id_cms_privileges' => 1,
                'sorting' => 2,
                'created_at' => '2018-01-22 10:22:02',
                'updated_at' => '2018-01-22 10:22:10',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Booking',
                'type' => 'URL',
                'path' => '#',
                'color' => 'normal',
                'icon' => 'fa fa-bookmark-o',
                'parent_id' => 0,
                'is_active' => 1,
                'is_dashboard' => 0,
                'id_cms_privileges' => 1,
                'sorting' => 3,
                'created_at' => '2018-01-27 22:06:49',
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Booking Belum Lunas',
                'type' => 'Route',
                'path' => 'bookings.belumlunas',
                'color' => 'normal',
                'icon' => 'fa fa-bookmark',
                'parent_id' => 6,
                'is_active' => 1,
                'is_dashboard' => 0,
                'id_cms_privileges' => 1,
                'sorting' => 2,
                'created_at' => '2018-01-29 04:06:28',
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Booking Lunas',
                'type' => 'Route',
                'path' => 'bookings.lunas',
                'color' => 'normal',
                'icon' => 'fa fa-bookmark',
                'parent_id' => 6,
                'is_active' => 1,
                'is_dashboard' => 0,
                'id_cms_privileges' => 1,
                'sorting' => 3,
                'created_at' => '2018-01-29 04:18:46',
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Booking Mendatang',
                'type' => 'Route',
                'path' => 'bookings.mendatang',
                'color' => 'normal',
                'icon' => 'fa fa-bookmark',
                'parent_id' => 6,
                'is_active' => 1,
                'is_dashboard' => 0,
                'id_cms_privileges' => 1,
                'sorting' => 4,
                'created_at' => '2018-01-29 04:48:00',
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Laporan DP',
                'type' => 'Route',
                'path' => 'laporan.dp',
                'color' => 'normal',
                'icon' => 'fa fa-file-text-o',
                'parent_id' => 14,
                'is_active' => 1,
                'is_dashboard' => 0,
                'id_cms_privileges' => 1,
                'sorting' => 2,
                'created_at' => '2018-01-29 07:44:54',
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Pembayarans',
                'type' => 'Route',
                'path' => 'AdminPembayaransControllerGetIndex',
                'color' => NULL,
                'icon' => 'fa fa-glass',
                'parent_id' => 0,
                'is_active' => 1,
                'is_dashboard' => 0,
                'id_cms_privileges' => 1,
                'sorting' => 4,
                'created_at' => '2018-02-01 10:01:00',
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Laporan Pelunasan',
                'type' => 'Route',
                'path' => 'laporan.pelunasan',
                'color' => 'normal',
                'icon' => 'fa fa-calendar-check-o',
                'parent_id' => 14,
                'is_active' => 1,
                'is_dashboard' => 0,
                'id_cms_privileges' => 1,
                'sorting' => 3,
                'created_at' => '2018-02-02 15:16:07',
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'Laporan Keuangan',
                'type' => 'Route',
                'path' => 'laporan.keuangan',
                'color' => 'normal',
                'icon' => 'fa fa-bar-chart',
                'parent_id' => 14,
                'is_active' => 1,
                'is_dashboard' => 0,
                'id_cms_privileges' => 1,
                'sorting' => 1,
                'created_at' => '2018-02-02 15:16:54',
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'Laporan',
                'type' => 'URL',
                'path' => '#',
                'color' => 'normal',
                'icon' => 'fa fa-copy',
                'parent_id' => 0,
                'is_active' => 1,
                'is_dashboard' => 0,
                'id_cms_privileges' => 1,
                'sorting' => 1,
                'created_at' => '2018-02-02 15:17:58',
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}