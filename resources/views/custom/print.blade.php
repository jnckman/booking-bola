<?php
 // Download printer driver for dot matrix printer ex. 1979 Dot Matrix      Regular or Consola
//oke
?>
<html>
<head>

<title>Print Booking Bola</title>
<style type="text/css" media="print">
.dontprint
{ display: none; }
</style>
<style>
@font-face { font-family: kitfont; src: url('1979 Dot Matrix Regular.TTF'); }

.customFont { /*  <div class="customFont" /> */
font-size:14pt;
font-family: monospace;
}
#mainDiv {
height: 500px; /* height of receipt 4.5 inches*/
width: 100%;  /* weight of receipt 8.6 inches*/
position:relative; /* positioned relative to its normal position */
}
#cqm { /*  <img id="cqm" /> */
top: 10px; /* top is distance from top (x axis)*/
left: 105px; /* left is distance from left (y axis)*/
position:absolute; /* position absolute based on "top" and "left"    parameters x and y  */
}
.clearfix {
    overflow: auto;
}
#or_mto { 
position: absolute;
left: 0px;
top: 0px;
z-index: -1; /*image */
}

    #arpno {
top: 80px;
left: 10px;
position:absolute;
}
#payee {
top: 80px;
left: 200px;
position:absolute;
}
#credit {
top: 80px;
right: 30px; /*   distance from right */
position:absolute;
}
#paydate {
top: 57px;
right: 120px;
position:absolute;
}
@media print {
  header,footer { 
    display: none; 
  }
}
@page{
	margin:0px;
}
 </style>


</head>
<body>

<div id="mainDiv"> <!--  invisible space -->
<div style="width: 100%;text-align: center;" class="customFont">
	BOOKING BOLA
</div>
<div style="width: 100%;text-align: center;" class="customFont">
	Jl. Alamat Bookings | url. booking bola .url
</div>
<br/>
<hr/>
<div style="width: 100%;" class="customFont">
	<span style="float:left;width: 50%;text-align: center;">{{$b->nomor}}</span>
	<span style='width:10%;float: left;text-align: left'>&nbsp;</span>
	<span style="float:right; width: 40%;">{{$b->tanggal}}</span>
</div>
<div style="width: 100%;" class="customFont">
	<span style="float:left;width: 50%;text-align: center;">{{ $b->lapangan }}</span>
	<span style='width:10%;float: left;text-align: left'>&nbsp;</span>
	<span style="float:right; width: 40%;">{{$b->durasi}} jam</span>
</div>
<div style="width: 30%;text-align: center;" class="customFont">
	
</div>
<div style="width: 100%;margin-top:20px;position: relative;" class="customFont clearfix">
</div>
<br/>
	<?php $tampung = '';?>
	    	<div style='width:20%;float: left;text-align: left'>
					&nbsp;
		    </div>
			<div style='width:30%;float: left;text-align: left'>
				{{ $b->pakets }} - {{ $b->pakets_jam }} jam
			</div>
			<div style="width:40%;float: right;text-align: left;">
				Rp {{ number_format($b->pakets_harga) }}
			</div>
	   
		
<div style="width: 100%;margin-top:20px;position: relative;" class="customFont clearfix">
</div>
<br>
<hr/>
<div style='width:100%;float: left;text-align: right;' class="customFont">
			<div style="width: 45%;float: left">TOTAL:</div><div style="width: 10%;float: left;">&nbsp;</div> <div style="width: 45%;float: right;text-align: left;">Rp {{ number_format($b->total) }}</div>
			<div style="width: 45%;float: left">DP:</div><div style="width: 10%;float: left;">&nbsp;</div> <div style="width: 45%;float: right;text-align: left;">Rp {{ number_format($b->dp) }}</div>
</div>
<div style="width: 100%;margin-top:20px;text-align: center;" class="customFont clearfix">
	<br>
	TERIMA KASIH 
</div>
<div style="width: 100%;text-align: center;" class="customFont">
	ATAS PEMBAYARAN ANDA
</div>

<div style="width: 100%;text-align: center" class="clearfix dontprint">
	<br/>
	<br/>
	<a href="{{ CRUDBooster::adminpath('bookings') }}"><button>KEMBALI KE NOTAS</button></a>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
</div>

</div>

</body>

<script src="{{ asset('vendor/crudbooster/assets/adminlte/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
	function printmz(){
		window.print();
		window.location = "{{url('admin/bookings')}}";
	}

	printmz();
});
</script>
</html>