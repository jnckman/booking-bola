@extends('crudbooster::admin_template')
@push('head')
<link rel="stylesheet" href="{{ asset ('css/select2.min.css') }}" >
<link rel="stylesheet" href="{{ asset ('css/bootstrap-datetimepicker.min.css') }}" >
<style type="text/css">
.bootstrap-datetimepicker-widget tr:hover {
    background-color: #808080;
}
</style>
@endpush
@section('content')
<div class="box box-success">
	<div class="box-header with-border">
		<form id="form1" class="form-inline">
			Filter : 
			<select class="form-control" id="jenisFilter" name="filter">
				<option value="tgl">Tanggal</option>
				<option value="m">Minggu</option>
				<option value="b">Bulan</option>
				<option value="thn">Tahun</option>
			</select>
			<div class="form-group">
				Tanggal : 
				<input type="text" id="daterange" name="daterange" class="form-control" placeholder="Filter tanggal" />
				<input type="text" id="weeklyDatePicker" class="form-control" placeholder="Filter minggu"  style="display: none" />
				<input type="text" id="monthPicker" class="form-control" placeholder="Filter bulan" style="display: none" />
				<input type="text" id="yearPicker" class="form-control" placeholder="Filter tahun" style="display: none" />
			</div>
			<button class="btn btn-md btn-info">Submit</button>
		</form>
	</div>
	<div class="box-body">
		<div class="table-responsive">
			<ul class="nav nav-tabs nav-justified">
                  <li class="active"><a data-toggle="tab" href="#tab_min"><h5><i class="ion-arrow-graph-up-right"></i> Pemasukan</h5></a></li>
                  <li ><a data-toggle="tab" href="#tab_max"><h5><i class="ion-arrow-graph-down-right"></i> Pengeluaran</h5></a></li>
            </ul>
            <div class="tab-content">
            	<br>
				<div id="tab_min" class="tab-pane active">
				<div class="table-responsive">
					<table id='table1' class="table table-hover table-bordered">
						<thead>
							<tr>
								<th>Tanggal</th>
								<th>Nomor</th>
								<th>Durasi</th>
								<th>Jam Mulai</th>
								<th>Jam Akhir</th>
								<th>Lapangan</th>
								<th>Pembooking</th>
								<th>Catatan</th>
								<th>Operator</th>
								<th>DP</th>
								<th>Lunas</th>
								<th>Tanggal Lunas</th>
								<th>Total</th>
								<th>Jam Booking</th>
								<th>Paket</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th colspan="9" class="text-right">Total</th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
							</tr>
						</tfoot>
					</table>
				</div>
				</div>
				<div id="tab_max" class="tab-pane">
				<div class="table-responsive">
					<table id='table2' class="table table-bordered">
						<thead>
							<tr>
								<th>Tanggal</th>
								<th>Operator</th>
								<th>Nominal</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th colspan="2" class="text-right">Total</th>
								<th></th>
							</tr>
						</tfoot>
					</table>
			</div>
		</div>
	</div>
</div>
@endsection
@push('bottom')
<script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
<script type="text/javascript">
$(function(){
	$("#daterange").daterangepicker({
		locale: {
		    format: 'YYYY-MM-DD'
	    },
	});
	

	$('#jenisFilter').on('change',function(){
		nil = $(this).val();
		if( nil == 'tgl'){
			$('#weeklyDatePicker').hide().val('').attr('name','');
			$('#monthPicker').hide().val('').attr('name','');
			$('#yearPicker').hide().val('').attr('name','');
			$('#daterange').show().val('').attr('name','daterange');
		}
		else if(nil == 'm'){
			$('#weeklyDatePicker').show().val('').attr('name','daterange');
			$('#daterange').hide().val('').attr('name','');
			$('#monthPicker').hide().val('').attr('name','');
			$('#yearPicker').hide().val('').attr('name','');
		}
		else if(nil == 'b'){
			$('#monthPicker').show().val('').attr('name','daterange');
			$('#daterange').hide().val('').attr('name','');
			$('#weeklyDatePicker').hide().val('').attr('name','');
			$('#yearPicker').hide().val('').attr('name','');
		}
		else{
			$('#yearPicker').show().val('').attr('name','daterange');
			$('#daterange').hide().val('').attr('name','');
			$('#weeklyDatePicker').hide().val('').attr('name','');
			$('#monthPicker').hide().val('').attr('name','');
		}
	});

	var table = $('#table1').DataTable({
		// data:[],
		processing: true,
		serverside:true,
		order:[],
		columns: [
		{ "data": "tanggal" },
		{ "data": "nomor" },
		{ "data": "durasi" },
		{ "data": "jam_mulai" },
		{ "data": "jam_akhir" },
		{ "data": "lapangans" },
		{ "data": "pembooking" },
		{ "data": "catatan" },
		{ "data": "operator" },
		{ "data": function (data, type, dataToSet) {
        	return data.dp.toLocaleString('en-EN');
    	}},
		{ "data": "lunas" },
		{ "data": "tanggal_lunas" },
		{ "data": function (data, type, dataToSet) {
			if($.isNumeric( data.total)){
	        	return parseInt(data.total).toLocaleString('en-EN');
			}
			else return data.total;
    	}},
		{ "data": "jam_booking" },
		{ "data": "paket" },
		],
		"footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
            
            total = api.column( 9 ) .data() .reduce( function (a, b) {return intVal(a) + intVal(b); }, 0 );
            total2 = api.column( 12 ) .data() .reduce( function (a, b) {return intVal(a) + intVal(b); }, 0 );
            $( api.column( 9 ).footer() ).html(total.toLocaleString('en-EN') );
            $( api.column( 12 ).footer() ).html(total2.toLocaleString('en-EN') );
        },
        columnDefs: [
            { "visible": false, "targets": 1 }
        ],
        order: [[ 1, 'asc' ],[0,'asc']],
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(1, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group" style="background-color:#EEEEEE"><td colspan="15"><b>'+group+'</b></td></tr>'
                    );
 
                    last = group;
                }
            } );
        },
	});

	var table2 = $('#table2').DataTable({
		// data:[],
		processing: true,
		serverside:true,
		order:[],
		columns: [
		{ "data": "tanggal" },
		{ "data": "operator" },
		{ "data": function (data, type, dataToSet) {
        	return data.nominal.toLocaleString('en-EN');
    	}},
		],
		"footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
            
            total = api.column( 2 ) .data() .reduce( function (a, b) {return intVal(a) + intVal(b); }, 0 );
            $( api.column( 2 ).footer() ).html(total2.toLocaleString('en-EN') );
        },
	});

	$('#form1').on('submit',function(e){
		e.preventDefault();
		$.ajax({
			url : "{{route('get_table_laporankeuangan')}}",
			type: 'get',
			data: $(this).serialize()
		}).done(function(response){
			table.clear().draw();
			table.rows.add(response.data1).draw();
			table2.clear().draw();
			table2.rows.add(response.data2).draw();
		}).fail(function(response){
			swal('Oops','Terjadi kesalahan','error');
		})
	});

	$('#monthPicker').datepicker( {
        format: "yyyy-mm",
        viewMode: "months", 
        minViewMode: "months"
    });

    $('#yearPicker').datepicker( {
		format: "yyyy",
		viewMode: "years", 
		minViewMode: "years"
	});

	$("#weeklyDatePicker").datetimepicker({
    	format: 'YYYY-MM-DD'
	});

	$('#weeklyDatePicker').on('dp.change', function (e) {
		var value = $("#weeklyDatePicker").val();
		var firstDate = moment(value, "YYYY-MM-DD").day(0).format("YYYY-MM-DD");
		var lastDate =  moment(value, "YYYY-MM-DD").day(6).format("YYYY-MM-DD");
		$("#weeklyDatePicker").val(firstDate + " - " + lastDate);
	});

});
</script>
@endpush