@extends('crudbooster::admin_template')
@push('head')
<link rel="stylesheet" href="{{ asset ('css/select2.min.css') }}" >
<link rel="stylesheet" href="{{ asset ('css/bootstrap-datetimepicker.min.css') }}" >
<style type="text/css">
.bootstrap-datetimepicker-widget tr:hover {
    background-color: #808080;
}
</style>
@endpush
@section('content')
<div class="box box-info">
	<div class="box-header with-border">
		<form id="form1" class="form-inline">
			Filter : 
			<select class="form-control" id="jenisFilter" name="filter">
				<option value="tgl">Tanggal</option>
				<option value="m">Minggu</option>
				<option value="b">Bulan</option>
				<option value="thn">Tahun</option>
			</select>
			<div class="form-group">
				Tanggal : 
				<input type="text" id="daterange" name="daterange" class="form-control" placeholder="Filter tanggal" />
				<input type="text" id="weeklyDatePicker" class="form-control" placeholder="Filter minggu"  style="display: none" />
				<input type="text" id="monthPicker" class="form-control" placeholder="Filter bulan" style="display: none" />
				<input type="text" id="yearPicker" class="form-control" placeholder="Filter tahun" style="display: none" />
			</div>
			<button class="btn btn-md btn-info">Submit</button>
		</form>
	</div>
	<div class="box-body">
		<div class="table-responsive">
			<table id='table1' class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Tanggal</th>
						<th>Nomor Booking</th>
						<th>Pembooking</th>
						<th>Jumlah</th>
						<th>Total</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th colspan="3" class="text-right">Total</th>
						<th></th>
						<th></th>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>
@endsection
@push('bottom')
<script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
<script type="text/javascript">
$(function(){
	$("#daterange").daterangepicker({
		locale: {
		    format: 'YYYY-MM-DD'
	    },
	});
	

	$('#jenisFilter').on('change',function(){
		nil = $(this).val();
		if( nil == 'tgl'){
			$('#weeklyDatePicker').hide().val('').attr('name','');
			$('#monthPicker').hide().val('').attr('name','');
			$('#yearPicker').hide().val('').attr('name','');
			$('#daterange').show().val('').attr('name','daterange');
		}
		else if(nil == 'm'){
			$('#weeklyDatePicker').show().val('').attr('name','daterange');
			$('#daterange').hide().val('').attr('name','');
			$('#monthPicker').hide().val('').attr('name','');
			$('#yearPicker').hide().val('').attr('name','');
		}
		else if(nil == 'b'){
			$('#monthPicker').show().val('').attr('name','daterange');
			$('#daterange').hide().val('').attr('name','');
			$('#weeklyDatePicker').hide().val('').attr('name','');
			$('#yearPicker').hide().val('').attr('name','');
		}
		else{
			$('#yearPicker').show().val('').attr('name','daterange');
			$('#daterange').hide().val('').attr('name','');
			$('#weeklyDatePicker').hide().val('').attr('name','');
			$('#monthPicker').hide().val('').attr('name','');
		}
	});

	var table = $('#table1').DataTable({
		// data:[],
		processing: true,
		serverside:true,
		order:[],
		columns: [
		{ "data": "tanggal" },
		{ "data": "nomor" },
		{ "data": "pembooking" },
		{ "data": function (data, type, dataToSet) {
        	return data.jumlah.toLocaleString('en-EN');
    	}},
		{ "data": function (data, type, dataToSet) {
        	return data.total.toLocaleString('en-EN');
    	}},
		],
		"footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
            
            total = api.column( 3 ) .data() .reduce( function (a, b) {return intVal(a) + intVal(b); }, 0 );
            total2 = api.column( 4 ) .data() .reduce( function (a, b) {return intVal(a) + intVal(b); }, 0 );
            $( api.column( 3 ).footer() ).html(total.toLocaleString('en-EN') );
            $( api.column( 4 ).footer() ).html(total2.toLocaleString('en-EN') );
        },
	});

	$('#form1').on('submit',function(e){
		e.preventDefault();
		$.ajax({
			url : "{{route('get_table_laporanpelunasan')}}",
			type: 'get',
			data: $(this).serialize()
		}).done(function(response){
			table.clear().draw();
			table.rows.add(response).draw();
		}).fail(function(response){
			swal('Oops','Terjadi kesalahan','error');
		})
	});

	$('#monthPicker').datepicker( {
        format: "yyyy-mm",
        viewMode: "months", 
        minViewMode: "months"
    });

    $('#yearPicker').datepicker( {
		format: "yyyy",
		viewMode: "years", 
		minViewMode: "years"
	});

	$("#weeklyDatePicker").datetimepicker({
    	format: 'YYYY-MM-DD'
	});

	$('#weeklyDatePicker').on('dp.change', function (e) {
		var value = $("#weeklyDatePicker").val();
		var firstDate = moment(value, "YYYY-MM-DD").day(0).format("YYYY-MM-DD");
		var lastDate =  moment(value, "YYYY-MM-DD").day(6).format("YYYY-MM-DD");
		$("#weeklyDatePicker").val(firstDate + " - " + lastDate);
	});

});
</script>
@endpush