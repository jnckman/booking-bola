@extends('crudbooster::admin_template')
@push('head')
<link rel="stylesheet" href="{{ asset ('css/fullcalendar.min.css') }}">
<link rel="stylesheet" href="{{ asset ('css/fullcalendar.print.css') }}" media='print'>
<link rel="stylesheet" href="{{ asset ('css/select2.min.css') }}" >
<link href="{{ asset('css/pace-theme-minimal.css')}}" rel="stylesheet" />
<style type="text/css">
	.fc-past{ background-color : #EEEEEE } 
	/*.fc-future{ background-color : blue  }*/
</style>
@endpush
@section('content')
<div class="row">
	<div class="col-md-4">
		<a href="{{route('bookings.belumlunas')}}" style="color: black !important">
		<div class="info-box">
			<span class="info-box-icon bg-orange"><i class="fa fa-calendar-times-o"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Booking Belum Lunas</span>
				<span class="info-box-number">{{$booking_belum_lunas}}</span>
			</div>
		</div>
		</a>
	</div>
	<div class="col-md-4">
		<div class="info-box">
			<span class="info-box-icon bg-aqua"><i class="ion-arrow-graph-up-right"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Total Pemasukan</span>
				<span class="info-box-number">Rp {{$pemasukan}}</span>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="info-box">
			<span class="info-box-icon bg-red"><i class="ion-arrow-graph-down-right"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Total Pengeluaran</span>
				<span class="info-box-number">Rp {{$pengeluarans}}</span>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		
		<h3></h3>
	</div>
	<div class="col-md-12" style="margin-bottom: 20px;">
		<div class="dropdown">
		  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
		    Pilih Lapangan
		    <span class="caret"></span>
		  </button>
		  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
		  <?php $lapangan = json_decode($lapangan); ?>
		  	@foreach($lapangan as $l)
		    <li><a href="admin/dashboard/{{$l->id}}">{{$l->text}}</a></li>
		    @endforeach
		  </ul>
		</div>
	</div>
	<div class="col-md-12">
	<div class="box box-default">
		<div class="box-header with-border">
			
		</div>
		<div class="box-body">
			<div id='calendar'></div>
		</div>
	</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-body">
				<canvas id="graph-pendapatan" width="350" height="100"></canvas>
			</div>
		</div>
	</div>
</div>

<div class="modal" id="modal_add_book" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
	    <div class="modal-content">
			<div class="modal-header">
				<h4 id="tanggalbooking"></h4>
				<h4 id="nomorbooking"></h4>
			</div>
			<form id="form1" class="form-horizontal">
				{{csrf_field()}}
				<input type="hidden" name="tanggal" id="inputtanggal">
			<div class="modal-body">
				<div class="form-group">
					<label class="col-sm-2 control-label">Pembooking:</label>
					<div class="col-sm-10">
						<input type="text" name="pembooking" class="form-control" placeholder="Masukkan nama pembooking" required="">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Mulai:</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="jam_mulai" id="inputmulai" readonly="" required="">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Selesai:</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="jam_akhir" id="inputselesai" readonly="" required="">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Durasi:</label>
					<div class="col-sm-10">
						<input type="number" id="inputdurasi" name="durasi" min="1" class="form-control" required="" step="1">
					</div>
					<p class="col-sm-10 col-sm-offset-2 help-block">Durasi dalam jam</p>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Paket:</label>
					<div class="col-sm-10">
						<select name="paket" id="selectPaket" style="width: 100%" required="">
							<option></option>
						</select>
					</div>
					<p class="col-sm-10 col-sm-offset-2 help-block">Paket berdasarkan durasi</p>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Lapangan:</label>
					<div class="col-sm-10">
						<select name="lapangans_id" id="selectLapangan" style="width: 100%" required="">
							<option></option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Catatan:</label>
					<div class="col-sm-10">
						<textarea name="catatan" placeholder="Masukkan catatan disini" class="form-control" rows="5" style="resize: none;"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">DP:</label>
					<div class="col-sm-10">
						<input type="text" name="dp" class="form-control" placeholder="Masukkan jumlah DP" required="">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Total:</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="totalbook_show" readonly="">
						<input type="hidden" name="total" class="form-control" id="totalbook" readonly="">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success btn-md" id="submit1">Submit</button>
				<button type="button" class="btn btn-default btn-md" data-dismiss="modal">Close</button>
			</div>
			</form>
		</div>
	</div>
</div>

<div class="modal" id="modal_view_book" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
	    <div class="modal-content">
			<div class="modal-header">
				<h5>Lihat Booking</h5>
				<h4></h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="col-sm-2 control-label">Pembooking:</label>
					<div class="col-sm-10">
						<pre name="pembooking"></pre>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Mulai:</label>
					<div class="col-sm-10">
						<pre name="jam_mulai"></pre>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Selesai:</label>
					<div class="col-sm-10">
						<pre name="jam_akhir"></pre>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Durasi:</label>
					<div class="col-sm-10">
						<pre name="durasi"></pre>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Paket:</label>
					<div class="col-sm-10">
						<pre name="paket"></pre>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Lapangan:</label>
					<div class="col-sm-10">
						<pre name="lapangan"></pre>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Catatan:</label>
					<div class="col-sm-10">
						<pre name="catatan"></pre>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">DP:</label>
					<div class="col-sm-10">
						<pre name="dp"></pre>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Total:</label>
					<div class="col-sm-10">
						<pre name="total"></pre>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-warning btn-md" id="btnedit">Edit</button>
				<button type="button" class="btn btn-default btn-md" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
@endsection
@push('bottom')
<script src="{{ asset('js/fullcalendar.min.js') }}"></script>
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{ asset('js/select2.min.js') }}"></script>
<script src="{{ asset('js/chart.bundle.min.js') }}"></script>
<script src="{{ asset('js/pace.min.js') }}"></script>
<script type="text/javascript">
$(function(){
	var mulai;
	var paket =  <?php echo $paket;?>;
	var selesai;
	var durasi=0;
	var clickEvent;
	var isedit = 0;
	$('#selectLapangan').select2({
		placeholder:'Pilih Lapangan',
		data: <?php echo $lapangan;?>,
	});
	$('#selectPaket').select2({
		placeholder:'Pilih Paket',
		// minimumResultsForSearch: Infinity,
	});

	var calendar = $('#calendar');

	calendar.fullCalendar({
		eventSources: [
	        {
	            url: '{{route("booking.ajaxgetbookings")}}',
	            type: 'get',
	            /*data: {
	                "start": calendar.fullCalendar('getView').start,
	                "end": calendar.fullCalendar('getView').end,
	            },*/
	            error: function() {
	                alert('there was an error while fetching events!');
	            },
	            // color: 'yellow',   // a non-ajax option
	            // textColor: 'black' // a non-ajax option
	        }
	    ],
	    timeFormat: 'H(:mm)',
        header: {
	        left: 'prev,next today',
	        center: 'title',
	        right: 'month,agendaWeek,agendaDay'
	    },
	    displayEventEnd:true,
		editable: false,
		eventLimit: true, // allow "more" link when too many events
		views:{
			agendaDay:{
				selectable : true,
			},
			agendaWeek:{
				selectable : true,	
			}
		},
		// selectConstraint:{
		//     start: '00:01', 
		//     end: '23:59', 
	 //    },
		eventClick: function(calEvent, jsEvent, view) {
			clickEvent = calEvent;
			modal = $('#modal_view_book');
			modal.find('h4').html( moment(calEvent.start.format()).format('DD-MMMM-YYYY') );
			modal.find('pre[name="jam_mulai"]').html( moment(calEvent.start.format()).format('HH:mm') );
			modal.find('pre[name="jam_akhir"]').html( moment(calEvent.end.format()).format('HH:mm') );
			modal.find('pre[name="durasi"]').html(calEvent.durasi);
			modal.find('pre[name="paket"]').html(calEvent.paket);
			modal.find('pre[name="lapangan"]').html(calEvent.lapangan);
			modal.find('pre[name="catatan"]').html(calEvent.catatan);
			modal.find('pre[name="pembooking"]').html(calEvent.pembooking);
			modal.find('pre[name="dp"]').html(calEvent.dp);
			modal.find('pre[name="total"]').html(calEvent.total);
			modal.find('h4').html(calEvent.nomor)
			modal.modal();
	    },
	    // eventAfterRender: function( event, element, view ) {
	    // 	// console.log(view);
	    // 	$(this).css('border-color', 'red');
	    // },
		dayRender: function (date, cell) {
            var today = new Date();
            var end = new Date();
            end.setDate(today.getDate()-1);
            if( date < end) {
            	cell.css("background-color", "#EEEEEE");
            } // this is for previous date 

            // if(date > today) {
            // 	cell.css("background-color", "blue");
            // }
        },
	    timezone: "local",
		ignoreTimezone: false,
		allDaySlot:false,
		dayClick: function(date, jsEvent, view, resourceObj) {
			if(view.name != 'month'){
				return;
			}
	    	calendar.fullCalendar('gotoDate',date);
	    	calendar.fullCalendar('changeView', 'agendaDay');
	    },
	    slotDuration:'01:00:00',
	    slotLabelFormat : 'HH:mm',
	    slotEventOverlap: false,
	    // eventOverlap: function(stillEvent, movingEvent) {
	    // 	alert('ada overlap')
	    //     return stillEvent.allDay && movingEvent.allDay;
	    // },
	    select: function( start, end, jsEvent, view ){
	    	// var m = $.fullCalendar.moment(start);
	    	// m.hasTime();
		    if(start.isBefore(moment())) {
		        $('#calendar').fullCalendar('unselect');
		        return false;
		    }
		    	mulai = moment(start.format());
		    	selesai = moment(end.format());
		    	durasi = selesai.diff(mulai,'hours');
		    	// if(durasi>1){
		    	// 	selesai = selesai.subtract(1,'hours');
		    	// 	durasi -= 1;
		    	// }
		    	$('#tanggalbooking').html(mulai.format('DD-MMMM-YYYY'));
		    	$('#inputtanggal').val(mulai.format('YYYY-MM-DD'));
		    	$('#inputmulai').val(mulai.format('HH:mm'));
		    	$('#inputselesai').val(selesai.format('HH:mm'));
		    	$('#inputdurasi').val(durasi);
		    	$('#inputdurasi').trigger('input');
		    	$('#modal_add_book').modal();
		    
	    }
    });

    $('#inputdurasi').on('input',function(){
    	durasi = $(this).val();
    	tampung = moment(mulai);
    	$('#inputselesai').val(tampung.add(durasi,'hours').format('HH:mm'));
    	//if (selesai < mulai ?)
    	selesai = tampung;
		var paketku = [];
		for (var i = 0; i < paket.length ; i++) {
			if(durasi % paket[i].jam  == 0){
				paketku.push(paket[i]);
			}
		}
		// $('#selectPaket').select2('clear');
		$('#selectPaket').select2().empty();
		$('#selectPaket').select2({
			data:paketku,
			placeholder:'Pilih Paket'
		});
		$('#selectPaket').val(null).trigger("change");
		// $('select').select2("val", null);
    });

    $(document).on('change','#selectPaket',function(e){
    	text = $('#selectPaket option:selected').text();
		gg = text.split("|");
		if(gg[1]>0){
			total = gg[1]*durasi;
			// $('input[name="dp"]').attr('max',total);
		}
		else{
			// $('input[name="dp"]').attr('max',0);	
			total = '';
		}
		$("#totalbook").val(total);
		$("#totalbook_show").val(total.toLocaleString());
    });

    $('#form1').submit(function(event){
    	event.preventDefault();
    	//kasih warning ketika jam selesai lebih dari 24
    	url = "{{route('booking.ajaxpostbooking')}}";
    	dpbaru = $('input[name="dp"]').val().replace(/,/g, "");
		$('input[name="dp"]').val(dpbaru);
    	if(isedit==1){
    		url = "{{route('booking.ajaxeditbooking')}}";
    		dataku = $('#form1').serialize() + "&id=" + clickEvent.id;
    	}
    	else{
    		dataku = $('#form1').serialize();	
    	}
    	if(!isSameDayAndMonth(selesai,mulai)){
    		swal('Warning','Check Durasi / Format !','warning');
    		return;
    	}
    	// return;
    	$.ajax({
    		type:'post',
    		url:url,
    		data: dataku,
    	}).done(function(response){
    		if(response.berhasil==0){
    			console.log(response);
    			swal('Oops','Jam bertabrakan','warning');
    		}
    		else if(response.berhasil==2){
    			console.log(response);
    			swal('Oops','Format jam mulai salah !','warning');	
    		}
    		else{
    			$('#form1').find("input, textarea").val("");
	    		$('#selectPaket').val(null).trigger("change");
	    		$('#selectLapangan').val(null).trigger("change");
	    		calendar.fullCalendar( 'refetchEvents' );
	    		swal('Success','Berhasil Menambahkan Data','success');
	    		$('#modal_add_book').modal('hide');
    		}
    	}).fail(function(response){
    		swal('Oops','Gagal Menambahkan Data','error');
    	})
    });

    function isSameDayAndMonth(m1, m2){
		return m1.date() === m2.date() && m1.month() === m2.month()
	}

	$('#modal_add_book').on('hide.bs.modal',function(){
		if(isedit==1){
			//reset form

		}
		isedit = 0;
	});

    $('#btnedit').click(function(){
    	console.log(clickEvent);
    	isedit = 1;
    	mulai = moment(clickEvent.start.format());
    	$('#inputtanggal').val( moment(clickEvent.start.format()).format('YYYY-MM-DD') );
    	$('#tanggalbooking').html( moment(clickEvent.start.format()).format('DD-MMMM-YYYY') );
    	$('#nomorbooking').html( clickEvent.nomor);
    	$('input[name="durasi"]').val(clickEvent.durasi);
    	$('input[name="durasi"]').trigger('input');
    	$('input[name="pembooking"').val(clickEvent.pembooking);
    	$('#inputmulai').attr('readonly',false);
    	$('input[name="jam_mulai"').val(moment(clickEvent.start.format()).format('HH:mm') );
		$('input[name="jam_akhir"').val( moment(clickEvent.end.format()).format('HH:mm') );
		$('textarea[name="catatan"]').val(clickEvent.catatan);
		$('input[name="dp"').val(clickEvent.dp);
		$('input[name="total"').val(clickEvent.total);
    	$('#modal_view_book').modal('hide');
    	$('#modal_add_book').modal();
    });

    $('#inputmulai').on('input',function(){
    	if(isedit==1){
    		mulai = moment($('#inputtanggal').val()+' '+$(this).val());
    		durasi = $('input[name="durasi"]').val();
	    	tampung = moment(mulai);
	    	$('#inputselesai').val(tampung.add(durasi,'hours').format('HH:mm'));
	    	selesai = tampung;
	    	// console.log(mulai);
    	}
    });


	var $input = $( "input[name='dp']" );
	$input.on( "input", function( event ) {
	 
	    // 1.
	    var selection = window.getSelection().toString();
	    if ( selection !== '' ) {
	        return;
	    }
	 
	    // 2.
	    if ( $.inArray( event.keyCode, [38,40,37,39] ) !== -1 ) {
	        return;
	    }

	    var $this = $( this );
		var input = $this.val();
		 
		// 2
		var input = input.replace(/[\D\s\._\-]+/g, "");
		 
		// 3
		input = input ? parseInt( input, 10 ) : 0;
		 
		// 4
		$this.val( function() {
		    return ( input === 0 ) ? "" : input.toLocaleString( "en-US" );
		} );
	} );

	$.ajax({
		url: "{{route('home.ajaxgraphpendapatan')}}"
	}).done(function(response){
		var config = {
            type: 'line',
            data: {
                labels: response.label,
                datasets: [{
                    label: "Pendapatan",
                    backgroundColor: '#3c8dbc',
                    borderColor: '#3c8dbc',
                    data: response.data,
                    fill: false,
                }]
            },
            options: {
                responsive: true,
                title:{
                    display:true,
                    text: 'Graph Pendapatan 6 Bulan Terakhir'
                },
                legend: {
			        display: false
			    },
			    tooltips: {
			        callbacks: {
			           label: function(tooltipItem) {
			                  return tooltipItem.yLabel;
			           }
			        }
			    },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Bulan'
                        },
                        gridLines: {
							display: false,
						},
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Rupiah'
                        },
                        ticks: {
						beginAtZero:true,
						userCallback: function(value, index, values) {
							if(value >=1000000000){
								value = value/1000000000 +' M';
								return value;
							}
							if(value >=1000000){
								value = value/1000000 +' jt';
								return value;
							}
							if(value >= 1000){
								value = value/1000 +' K';
								return value;
							}
						}  
						},
                    }]
                }
            }
        };
        var ctx = $('#graph-pendapatan').get(0).getContext('2d');
	    var chartline = new Chart(ctx, config);
	}).fail(function(){
		console.log('Gagal mengambil data chart');
	});
});
</script>
@endpush