function bayar(id){
	$.ajax({
		data:{'byId':id},
		url: 'bookings/ajaxgetbookings',
        type: 'get',
	}).done(function(response){
		tambahbayars = 0;
		data = JSON.parse(response);
		// console.log(data.bayars);
		$('#show_nomor').html(data.hasil[0].nomor);
		$('#show_pembooking').html(data.hasil[0].pembooking);
		$('#show_tanggal').html(data.hasil[0].start.split(' ')[0]);
		$('#show_durasi').html(data.hasil[0].durasi+' jam');
		$('#show_waktu').html(data.hasil[0].start.split(' ')[1]+' - '+data.hasil[0].end.split(' ')[1]);
		$('#show_paket').html(data.hasil[0].paket);
		$('#show_lapangan').html(data.hasil[0].lapangan);
		$('#show_catatan').html(data.hasil[0].catatan);
		$('#show_dp').html(data.hasil[0].dp.toLocaleString());
		$('#show_total').html(data.hasil[0].total.toLocaleString());
		if(data.bayars.length > 0){
			$('#listbayars').html('');
			for (var i = 0; i < data.bayars.length; i++) {
				$('#listbayars').append('<div class="row">'+
							'<div class="col-xs-2 col-xs-offset-6">'+
								'Bayar '+(i+1)+':'+
							'</div>'+
							'<div class="col-xs-4">'+
								'<span>'+data.bayars[i].jumlah.toLocaleString()+'</span>'+
							'</div>'+
						'</div>');
				tambahbayars += data.bayars[i].jumlah;
			}
		}
		$('#show_kurang').html((data.hasil[0].total - data.hasil[0].dp - tambahbayars).toLocaleString());
		$('input[name="bookings_id"]').val(data.hasil[0].id);
		$('#modalBayar').modal();
	}).fail(function(){
		swal('Oops','terjadi kesalahan','error');
	});
}

$(function(){
	// alert('duh');
	var $input = $( "input[name='jumlah']" );
	$input.on( "input", function( event ) {
	 
	    // 1.
	    var selection = window.getSelection().toString();
	    if ( selection !== '' ) {
	        return;
	    }
	 
	    // 2.
	    if ( $.inArray( event.keyCode, [38,40,37,39] ) !== -1 ) {
	        return;
	    }

	    var $this = $( this );
		var input = $this.val();
		 
		// 2
		var input = input.replace(/[\D\s\._\-]+/g, "");
		 
		// 3
		input = input ? parseInt( input, 10 ) : 0;
		 
		// 4
		$this.val( function() {
		    return ( input === 0 ) ? "" : input.toLocaleString( "en-US" );
		} );
	} );

	$('#formbayar').on('submit',function(ee){
		ee.preventDefault();
        wth = $('input[name="jumlah"]').val().replace(/,/g, "");
        $('input[name="jumlah"]').val(wth);
        $.ajax({
            type: 'post',
            url : 'booking/postbayar',
            data : $('#formbayar').serialize(),
        }).success(function(response){
            console.log(response);
            if(response.berhasil==1){
                $('#modalBayar').modal('hide');
                $('#modalProgress').modal('hide');
                swal("Success!", "Berhasil!", "success")
            }
            else if(response.berhasil==2){
            	swal('Oops','Input Terlalu banyak','warning');
            }
            else{
                swal('Oops','Gagal Mengirim Data','error');
            }
        }).fail(function(){
            swal('Oops','Terjadi Kesalahan','error');
        });
	});
});