<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;

	class AdminBookingsController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "id";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = false;
			$this->button_show = true;
			$this->button_filter = true;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "bookings";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Nomor","name"=>"nomor"];
			$this->col[] = ["label"=>"Tanggal","name"=>"tanggal"];
			$this->col[] = ["label"=>"Durasi","name"=>"durasi"];
			$this->col[] = ["label"=>"Jam Mulai","name"=>"jam_mulai"];
			$this->col[] = ["label"=>"Jam Akhir","name"=>"jam_akhir"];
			$this->col[] = ["label"=>"Catatan","name"=>"catatan"];
			$this->col[] = ["label"=>"Lapangan","name"=>"lapangans_id","join"=>"lapangans,nama"];
			$this->col[] = ["label"=>"Pembooking","name"=>"pembooking"];
			$this->col[] = ["label"=>"Operator","name"=>"operator","join"=>'cms_users,name'];
			$this->col[] = ["label"=>"Dp","name"=>"dp"];
			$this->col[] = ["label"=>"Lunas","name"=>"lunas"];
			$this->col[] = ["label"=>"Tanggal Lunas","name"=>"tanggal_lunas"];
			$this->col[] = ["label"=>"Total","name"=>"total"];
			$this->col[] = ["label"=>"Jam Booking","name"=>"jam_booking"];
			$this->col[] = ["label"=>"Pakets","name"=>"pakets_id","join"=>"pakets,nama"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Tanggal','name'=>'tanggal','type'=>'date','validation'=>'required|date','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Durasi','name'=>'durasi','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Jam Mulai','name'=>'jam_mulai','type'=>'time','validation'=>'required|date_format:H:i:s','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Jam Akhir','name'=>'jam_akhir','type'=>'time','validation'=>'required|date_format:H:i:s','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Catatan','name'=>'catatan','type'=>'textarea','validation'=>'string|max:5000','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Lapangans Id','name'=>'lapangans_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'lapangans,nama'];
			$this->form[] = ['label'=>'Pembooking','name'=>'pembooking','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Operator','name'=>'operator','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'cms_users,name'];
			$this->form[] = ['label'=>'Dp','name'=>'dp','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Lunas','name'=>'lunas','type'=>'select','validation'=>'required|string|min:0','width'=>'col-sm-10','dataenum'=>'belum;lunas'];
			$this->form[] = ['label'=>'Tanggal Lunas','name'=>'tanggal_lunas','type'=>'date','validation'=>'date','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Total','name'=>'total','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Jam Booking','name'=>'jam_booking','type'=>'time','validation'=>'required|date_format:H:i:s','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Pakets Id','name'=>'pakets_id','type'=>'select2','validation'=>'integer|min:0','width'=>'col-sm-10','datatable'=>'pakets,nama'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Tanggal','name'=>'tanggal','type'=>'date','validation'=>'required|date','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Durasi','name'=>'durasi','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Jam Mulai','name'=>'jam_mulai','type'=>'time','validation'=>'required|date_format:H:i:s','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Jam Akhir','name'=>'jam_akhir','type'=>'time','validation'=>'required|date_format:H:i:s','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Catatan','name'=>'catatan','type'=>'textarea','validation'=>'string|max:5000','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Lapangans Id','name'=>'lapangans_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'lapangans,nama'];
			//$this->form[] = ['label'=>'Pembooking','name'=>'pembooking','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Operator','name'=>'operator','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'cms_users,name'];
			//$this->form[] = ['label'=>'Dp','name'=>'dp','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Lunas','name'=>'lunas','type'=>'text','validation'=>'required|string|min:0','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Tanggal Lunas','name'=>'tanggal_lunas','type'=>'date','validation'=>'date','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Total','name'=>'total','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Jam Booking','name'=>'jam_booking','type'=>'time','validation'=>'required|date_format:H:i:s','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Pakets Id','name'=>'pakets_id','type'=>'select2','validation'=>'integer|min:0','width'=>'col-sm-10','datatable'=>'pakets,nama'];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();
	        $this->addaction[] = ['label'=>'','icon'=>'fa fa-money btn_bayar','color'=>'primary','url'=>'javascript:bayar([id])','showIf'=>'[lunas] != "lunas"'];
	        $this->addaction[] = ['label'=>'','icon'=>'fa fa-print','color'=>'info','url'=>CRUDBooster::mainpath('print/[id]'),'showIf'=>'[lunas] == "lunas"'];

	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        $this->post_index_html = '
	        <div class="modal" id="modalBayar" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog">
				<div class="modal-content">
					<form id="formbayar">
					<input type="hidden" name="bookings_id" />
					<div class="modal-header">
						<h4 id="show_nomor"></h4>
						<div class="row">
							<div class="col-sm-12">
								<span id="show_tanggal"></span>
								a/n
								<span id="show_pembooking"></span>
							</div>
						</div>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-sm-2">
								Durasi 
							</div>
							<div class="col-sm-4">
							<span id="show_durasi"></span>
							</div>
							<div class="col-sm-4">
								<span id="show_waktu"> </span>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-sm-2">
								Paket / Lapangan
							</div>
							<div class="col-sm-4">
								<span id="show_paket"></span>
							</div>
							<div class="col-sm-6">
								<span id="show_lapangan"></span>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-sm-2">
								Catatan
							</div>
							<div class="col-sm-10">
								<pre id="show_catatan"></pre>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-xs-2">
								DP:
							</div>
							<div class="col-xs-4">
								<span id="show_dp"> </span>
							</div>
							<div class="col-xs-2">
								Total:
							</div>
							<div class="col-xs-4">
								<span id="show_total"> </span>
							</div>
						</div>
						<div id="listbayars">
						</div>
						<div class="row">
							<div class="col-xs-2 col-xs-offset-6">
								Kurang:
							</div>
							<div class="col-xs-4">
								<span id="show_kurang"></span>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-xs-2">
								Bayar:
							</div>
							<div class="col-xs-10">
								<input type="text" name="jumlah" class="form-control" placeholder="0" required="" />
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-success btn-md">Submit</button>
						<button type="button" class="btn btn-default btn-md" data-dismiss="modal">Close</button>
					</div>
					</form>
				</div>
				</div>
			</div>
	        ';
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        $this->load_js[] = asset('js/bookings.js');
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	        $request= request();
	        if(isset($request->mendatang) && $request->mendatang==1){
	        	$query = $query->where('tanggal','>=',date('Y-m-d'));
	        }
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	if($column_index==10 || $column_index==13){
	    		$column_value = number_format($column_value);
	    	}
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here
	    	$nomor = 'B-'.date('Ymd').$id;
	    	DB::table('bookings')->where('id',$id)->update(['nomor'=>$nomor]);
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }


	    //By the way, you can still create your own method in here... :) 
	    public function gethomebylapangan($id){
	    	$data = [];
	    	$data['page_title'] = 'Home';
	    	$data['lapangan'] = DB::table('lapangans')
	    							->whereNull('deleted_at')
	    							->select('id','nama as text')
	    							->get();

			$data['paket'] = DB::table('pakets')
	    							->whereNull('deleted_at')
	    							->select('id',DB::raw('CONCAT(nama," | ",harga," | ",jam," jam") as text'),'jam','harga')
	    							->get();

	    	$bookings_minggu = DB::table('bookings')
	    										->whereBetween('tanggal', [
												    date('Y-m-d', strtotime('this week Monday')),
													date('Y-m-d', strtotime('next week Sunday'))
												])
												->where('lapangans_id',$id)
	    										->whereNull('deleted_at')
	    										->get();
	    	$data['booking_belum_lunas'] = $bookings_minggu->where('lunas','belum')->count();

	    	$pemasukan = 0;
	    	foreach ($bookings_minggu as $key => $value) {
	    		$pemasukan += $value->dp;
	    	}

	    	$masuk = DB::table('pembayarans')->whereBetween('tanggal', [
											    date('Y-m-d', strtotime('this week Monday')),
												date('Y-m-d', strtotime('next week Sunday'))
											])
									    	->whereNull('deleted_at')
									    	->sum('jumlah');
			$pemasukan += $masuk;
			$keluar = DB::table('pengeluarans')->whereBetween('tanggal', [
											    date('Y-m-d', strtotime('this week Monday')),
												date('Y-m-d', strtotime('next week Sunday'))
											])
									    	->whereNull('deleted_at')
									    	->sum('nominal');
	    	$data['pengeluarans'] = number_format($pengeluarans);
			$data['pemasukan'] = number_format($pemasukan);
	    	$data['lapangan'] = json_encode($data['lapangan']);
	    	$data['paket'] = json_encode($data['paket']);
	    	$data['berdasarlapangan'] = $id;
	    	$this->cbView('custom.gethome',$data);
	    }
	    public function getHome(){
	    	$data = [];
	    	$data['page_title'] = 'Home';
	    	$data['lapangan'] = DB::table('lapangans')
	    							->whereNull('deleted_at')
	    							->select('id','nama as text')
	    							->get();

			$data['paket'] = DB::table('pakets')
	    							->whereNull('deleted_at')
	    							->select('id',DB::raw('CONCAT(nama," | ",harga," | ",jam," jam") as text'),'jam','harga')
	    							->get();

	    	$bookings_minggu = DB::table('bookings')
	    										->whereBetween('tanggal', [
												    date('Y-m-d', strtotime('this week Monday')),
													date('Y-m-d', strtotime('next week Sunday'))
												])
	    										->whereNull('deleted_at')
	    										->get();
	    	$data['booking_belum_lunas'] = $bookings_minggu->where('lunas','belum')->count();

	    	$pemasukan = 0;
	    	foreach ($bookings_minggu as $key => $value) {
	    		$pemasukan += $value->dp;
	    	}

	    	$masuk = DB::table('pembayarans')->whereBetween('tanggal', [
											    date('Y-m-d', strtotime('this week Monday')),
												date('Y-m-d', strtotime('next week Sunday'))
											])
									    	->whereNull('deleted_at')
									    	->sum('jumlah');
			$pemasukan += $masuk;
			$keluar = DB::table('pengeluarans')->whereBetween('tanggal', [
											    date('Y-m-d', strtotime('this week Monday')),
												date('Y-m-d', strtotime('next week Sunday'))
											])
									    	->whereNull('deleted_at')
									    	->sum('nominal');
	    	$data['pengeluarans'] = number_format($pengeluarans);
			$data['pemasukan'] = number_format($pemasukan);
	    	$data['lapangan'] = json_encode($data['lapangan']);
	    	$data['paket'] = json_encode($data['paket']);
	    	$data['berdasarlapangan'] = 0;
	    	$this->cbView('custom.gethome',$data);
	    }

	    public function ajaxpostbooking(){
	    	$request = request();
	    	// dd($request);
	    	$request->dp = (int)$request->dp;
	    	// dd($request);
	    	$jadwal_sama = DB::table('bookings')
	    						->where('tanggal',$request->tanggal)
	    						->where('lapangans_id',$request->lapangans_id)
	    						->whereNull('deleted_at')
	    						->get()
	    						;
	    	if(count($jadwal_sama)>0){
	    		foreach ($jadwal_sama as $key => $v) {
	    			if( $request->jam_mulai < $v->jam_mulai && $request->jam_akhir <= $v->jam_mulai ) 
	    			{
	    				continue;
	    			}
	    			else if($request->jam_mulai >= $v->jam_akhir){
	    				continue;
	    			}
	    			else{
	    				return response()->json(['berhasil'=>0,'request'=>[$request->jam_mulai,$request->jam_akhir],'jadwal_sama'=>[$v->jam_mulai,$v->jam_akhir]]);
	    			}
	    		}
	    	}
	    	if($request->total == $request->dp){
	    		$lunas = 'lunas';
	    		$tanggal_lunas = date('Y-m-d');
	    	} 
		    else {
		    	$lunas = 'belum';
		    	$tanggal_lunas = null;
		    } 
	    	$idku = DB::table('bookings')->insertGetId(['created_at'=>now(),
				    									'tanggal'=>$request->tanggal,
				    									'durasi'=>$request->durasi,
				    									'jam_mulai'=>$request->jam_mulai,
				    									'jam_akhir'=>$request->jam_akhir,
				    									'catatan'=>$request->catatan,
				    									'lapangans_id'=>$request->lapangans_id,
				    									'pembooking'=>$request->pembooking,
				    									'operator'=>CRUDBooster::myId(),
				    									'dp'=>$request->dp,
				    									'lunas'=>$lunas,
				    									'tanggal_lunas'=>$tanggal_lunas,
				    									'jam_booking'=>date('H:i:s'),
				    									'pakets_id'=>$request->paket,
				    									'total'=>$request->total,
					    								]);
	    	$nomor = 'B-'.date('Ymd').$idku;
	    	DB::table('bookings')->where('id',$idku)->update(['nomor'=>$nomor]);
	    	return response()->json(['berhasil'=>1]);
	    }

	    public function ajaxeditbooking(){
	    	$request = request();
	    	$request->dp = (int)$request->dp;
	    	$jadwal_sama = DB::table('bookings')
	    						->where('tanggal',$request->tanggal)
	    						->where('lapangans_id',$request->lapangans_id)
	    						;

	    	if(isset($request->id)){
	    		$jadwal_sama =  $jadwal_sama->where('id','!=',$request->id);
	    	}

	    	$jadwal_sama = $jadwal_sama->whereNull('deleted_at')
		    							->get()
		    							;

	    	$format_waktu = preg_match("/(2[0-4]|[01][1-9]|10):([0-5][0-9])/", $request->jam_mulai);
	    	if($format_waktu==0){
	    		return response()->json(['berhasil'=>2]);
	    	}

	    	if(count($jadwal_sama)>0){
	    		foreach ($jadwal_sama as $key => $v) {
	    			if( $request->jam_mulai < $v->jam_mulai && $request->jam_akhir <= $v->jam_mulai ) 
	    			{
	    				continue;
	    			}
	    			else if($request->jam_mulai >= $v->jam_akhir){
	    				continue;
	    			}
	    			else{
	    				return response()->json(['berhasil'=>0,'request'=>[$request->jam_mulai,$request->jam_akhir],'jadwal_sama'=>[$v->jam_mulai,$v->jam_akhir]]);
	    			}
	    		}
	    	}
	    	if($request->total == $request->dp){
	    		$lunas = 'lunas';
	    		$tanggal_lunas = date('Y-m-d');
	    	} 
		    else {
		    	$lunas = 'belum';
		    	$tanggal_lunas = null;
		    }

		    DB::table('bookings')->where('id',$request->id)->update(['updated_at'=>now(),
		    														'durasi'=>$request->durasi,
							    									'jam_mulai'=>$request->jam_mulai,
							    									'jam_akhir'=>$request->jam_akhir,
							    									'catatan'=>$request->catatan,
							    									'lapangans_id'=>$request->lapangans_id,
							    									'pembooking'=>$request->pembooking,
							    									'operator'=>CRUDBooster::myId(),
							    									'dp'=>$request->dp,
							    									'lunas'=>$lunas,
							    									'tanggal_lunas'=>$tanggal_lunas,
							    									'jam_booking'=>date('H:i:s'),
							    									'pakets_id'=>$request->paket,
							    									'total'=>$request->total,
		    														]);
	    	return response()->json(['berhasil'=>1]);
	    }

	    public function ajaxgetbookings(){
	    	$request = request();
	    	// dd($request);
	    	$hasil = DB::table('bookings')
	    				->leftJoin('lapangans','lapangans.id','=','bookings.lapangans_id')
	    				->leftJoin('pakets','pakets.id','=','bookings.pakets_id');
	    	if(isset($request->byId) ){
	    		$hasil = $hasil->where('bookings.id',$request->byId);
	    		$bayars = DB::table('pembayarans')
	    					->where('bookings_id',$request->byId)
	    					->get()
	    					;
	    	}
			else{
				$hasil = $hasil->whereDate('bookings.tanggal','>=',$request->start)
	    				->whereDate('bookings.tanggal','<=',$request->end);
			}
	    	$hasil = $hasil->whereNull('bookings.deleted_at')
	    				->select('bookings.id'
	    					,'bookings.pembooking as title'
	    					,DB::raw('CONCAT(bookings.tanggal," ",jam_mulai) as start')
	    					,DB::raw('CONCAT(bookings.tanggal," ",jam_akhir) as end')
	    					,'bookings.deleted_at'
	    					,'bookings.durasi'
	    					,'pakets.nama as paket'
	    					,'lapangans.nama as lapangan'
	    					,'bookings.pembooking'
	    					,'bookings.dp'
	    					,'bookings.total'
	    					,'bookings.catatan'
	    					,'bookings.lunas'
	    					,'lapangans.color'
	    					,'bookings.lapangans_id'
	    					,'bookings.nomor'
	    				)
	    				->get()
	    				;
	    	// dd($hasil);
			if(isset($request->byId) ){
				return json_encode(['hasil'=>$hasil,'bayars'=>$bayars]);
			}
			else{
				return json_encode($hasil);
			}
	    }

	    public function ajaxgetpaket(){
	    	$request = request();

	    }

	    public function postBayar(){
	    	$request = request();
	    	$data = DB::table('pembayarans')
	    				->leftJoin('bookings','bookings.id','=','pembayarans.bookings_id')
				    	->where('bookings.id',$request->bookings_id)
				    	->whereNull('pembayarans.deleted_at')
				    	->get();

			$data2 = DB::table('bookings')
							->where('id',$request->bookings_id)
							->first();

			if(count($data)==0){
				$total = (int)$request->jumlah + (int)$data2->dp;
				if($total > $data2->total){
					return response()->json(['berhasil'=>2,'total'=>$total,'jnck'=>$request->bookings_id]);
				}
				if($total == $data2->total){
					$lunas = 'lunas';
				}
				else{
					$lunas = 'belum';	
				}
			}
			else{
				$jmlh = $data->sum('jumlah');
				$total = (int)$jmlh + (int)$request->jumlah + (int)$data2->dp;
				if($total > $data2->total ){
					return response()->json(['berhasil'=>2,'total'=>$total,'jnck'=>2]);
				}
				if($total == $data2->total ){
					$lunas = 'lunas';
				}
				else{
					$lunas = 'belum';	
				}
			}
	    	DB::table('pembayarans')->insert(['created_at'=>now(),
	    										'bookings_id'=>$data2->id,
	    										'jumlah'=>$request->jumlah,
	    										'tanggal'=>date('Y-m-d')
	    									]);
	    	// return response()->json(['berhasil'=>1]);
	    	DB::table('bookings')->where('id',$data2->id)
							    ->update([
							    	'updated_at'=>now(),
	    							'lunas'=>$lunas
	    									])
							    ;
	    	return response()->json(['berhasil'=>1]);
	    }

	    public function ajaxgraphpendapatan(){
	    	// $hasil = DB::table()
	  //   	$hasil = DB::table('bookings')
			// 	    	->whereNull('deleted_at')
			// 	    	->where('tanggal','>=',date('Y-m-d', strtotime('-6 month')) )
			// 	    	->select(DB::raw('MONTHNAME(tanggal) as tanggal'),DB::raw('SUM(dp) as jumlah'))
			// 	    	->groupBy(DB::raw('MONTHNAME(tanggal)'))
			// 	    	// ->get()
			// 	    	;
			// $hasil2 = DB::table('pembayarans')
			// 			->whereNull('deleted_at')
			// 			->where('tanggal','>=',date('Y-m-d', strtotime('-6 month')) )
			// 	    	->select(DB::raw('MONTHNAME(tanggal) as tanggal'),DB::raw('SUM(jumlah) as jumlah'))
			// 	    	->groupBy(DB::raw('MONTHNAME(tanggal)'))
			// 	    	->union($hasil)
			// 			->get()
			// 			;
			$hasil3 = DB::table('bookings')
						->leftJoin('pembayarans',function($q){
							$q->on('pembayarans.bookings_id','=','bookings.id');
							$q->where('pembayarans.tanggal','>=',date('Y-m-d', strtotime('-6 month')) );
						})
				    	->whereNull('bookings.deleted_at')
				    	->whereNull('pembayarans.deleted_at')
				    	->where('bookings.tanggal','>=',date('Y-m-d', strtotime('-6 month')) )
				    	->select(
				    		DB::raw('MONTHNAME(bookings.tanggal) as tanggal')
				    		,DB::raw('MONTH(bookings.tanggal) as bulan')
				    		,DB::raw('YEAR(bookings.tanggal) as tahun')
				    		,DB::raw('SUM(bookings.dp) as jumlah'),DB::raw('SUM(pembayarans.jumlah) as jumlah2')
				    		,DB::raw('COALESCE(SUM(bookings.dp),0)+COALESCE(SUM(pembayarans.jumlah),0) AS jumlah')
				    	)
				    	->groupBy(
				    		'bulan'
				    		,DB::raw('MONTHNAME(bookings.tanggal)')
				    		,'tahun')
				    	->orderby('tahun')
				    	->orderby('bulan')
				    	->get()
				    	;
			// dd($hasil3);
			$label = $hasil3->pluck('tanggal');
			$data = $hasil3->pluck('jumlah');
			// dd($data);
	    	return response()->json(['label'=>$label,'data'=>$data]);
	    }

	    public function laporandp(){
	    	$data =[];
	    	$datap['page_title'] = 'Laporan DP';

	    	$this->cbView('custom.laporan_dp',$data);
	    }

	    public function get_table_laporandp(){
	    	$request = request();
	    	$hasil = DB::table('bookings')
	    				->leftJoin('lapangans','lapangans.id','=','bookings.lapangans_id')
	    				->leftJoin('cms_users','cms_users.id','=','bookings.operator')
	    				->leftJoin('pakets','pakets.id','=','bookings.pakets_id')
	    				;
	    	if($request->filter == 'b'){
	    		$tanggal[0] = $request->daterange.'-01';
				$tanggal[2] = $request->daterange.'-'.date('t',strtotime($tanggal[0]));
				$hasil = $hasil->whereDate('bookings.tanggal','>=',$tanggal[0])
	    				->whereDate('bookings.tanggal','<=',$tanggal[2]);
	    	}
	    	else if($request->filter == 'thn'){
	    		$hasil = $hasil->whereYear('bookings.tanggal','=',$request->daterange);
	    	}
	    	else{
	    		$tanggal = explode(' ', $request->daterange);
	    		$hasil = $hasil->whereDate('bookings.tanggal','>=',$tanggal[0])
	    				->whereDate('bookings.tanggal','<=',$tanggal[2]);
	    	}
			$hasil=$hasil->whereNull('bookings.deleted_at')
	    				->select('bookings.*','lapangans.nama as lapangans','cms_users.name as operator','pakets.nama as paket')
	    				->get()
	    				->toArray()
	    				;

	    	return response()->json($hasil);
	    }
	    
	    public function pelunasan(){
	    	$data =[];
	    	$datap['page_title'] = 'Pelunasan';

	    	$this->cbView('custom.laporan_pelunasan',$data);
	    }

	    public function get_table_laporanpelunasan(){
	    	$request = request();
	    	$hasil = DB::table('pembayarans')
	    				->leftJoin('bookings','bookings.id','=','pembayarans.bookings_id')
	    				;
	    	if($request->filter == 'b'){
	    		$tanggal[0] = $request->daterange.'-01';
				$tanggal[2] = $request->daterange.'-'.date('t',strtotime($tanggal[0]));
				$hasil = $hasil->whereDate('pembayarans.tanggal','>=',$tanggal[0])
	    				->whereDate('pembayarans.tanggal','<=',$tanggal[2]);
	    	}
	    	else if($request->filter == 'thn'){
	    		$hasil = $hasil->whereYear('pembayarans.tanggal','=',$request->daterange);
	    	}
	    	else{
	    		$tanggal = explode(' ', $request->daterange);
	    		$hasil = $hasil->whereDate('pembayarans.tanggal','>=',$tanggal[0])
	    				->whereDate('pembayarans.tanggal','<=',$tanggal[2]);
	    	}
			$hasil=$hasil->whereNull('bookings.deleted_at')
	    				->select('bookings.nomor','bookings.pembooking','pembayarans.tanggal','pembayarans.jumlah','bookings.total')
	    				->get()
	    				->toArray()
	    				;

	    	return response()->json($hasil);
	    }

	    public function keuangan(){
	    	$data =[];
	    	$datap['page_title'] = 'Keuangan';

	    	$this->cbView('custom.laporan_keuangan',$data);
	    }	

	    public function get_table_laporankeuangan(){
	    	// dd('oke');
	    	$request = request();
	    	$data2 = DB::table('pengeluarans')
	    				->leftJoin('cms_users','cms_users.id','=','pengeluarans.operator')
	    				;

	    	$hasil = DB::table('bookings')
	    				->leftJoin('lapangans','lapangans.id','=','bookings.lapangans_id')
	    				->leftJoin('cms_users','cms_users.id','=','bookings.operator')
	    				->leftJoin('pakets','pakets.id','=','bookings.pakets_id')
	    				;

			$data1 = DB::table('pembayarans')
	    				->leftJoin('bookings','bookings.id','=','pembayarans.bookings_id')
	    				;

	    	if($request->filter == 'b'){
	    		$tanggal[0] = $request->daterange.'-01';
				$tanggal[2] = $request->daterange.'-'.date('t',strtotime($tanggal[0]));
				$data2 = $data2->whereDate('pengeluarans.tanggal','>=',$tanggal[0])
	    				->whereDate('pengeluarans.tanggal','<=',$tanggal[2]);
				$hasil = $hasil->whereDate('bookings.tanggal','>=',$tanggal[0])
	    				->whereDate('bookings.tanggal','<=',$tanggal[2]);
	    		$data1 = $data1->whereDate('pembayarans.tanggal','>=',$tanggal[0])
	    				->whereDate('pembayarans.tanggal','<=',$tanggal[2]);
	    	}
	    	else if($request->filter == 'thn'){
	    		$data2 = $data2->whereYear('pengeluarans.tanggal','=',$request->daterange);
	    		$hasil = $hasil->whereYear('bookings.tanggal','=',$request->daterange);
	    		$data1 = $data1->whereYear('pembayarans.tanggal','=',$request->daterange);
	    	}
	    	else{
	    		$tanggal = explode(' ', $request->daterange);
	    		$data2 = $data2->whereDate('pengeluarans.tanggal','>=',$tanggal[0])
	    				->whereDate('pengeluarans.tanggal','<=',$tanggal[2]);
				$hasil = $hasil->whereDate('bookings.tanggal','>=',$tanggal[0])
	    				->whereDate('bookings.tanggal','<=',$tanggal[2]);
	    		$data1 = $data1->whereDate('pembayarans.tanggal','>=',$tanggal[0])
	    				->whereDate('pembayarans.tanggal','<=',$tanggal[2]);
	    	}
			$data2=$data2->whereNull('pengeluarans.deleted_at')
						->select('pengeluarans.tanggal','pengeluarans.nominal','cms_users.name as operator')
	    				->get()
	    				->toArray()
	    				;

	    	$hasil=$hasil->whereNull('bookings.deleted_at')
	    				->select(
	    					'bookings.nomor'
	    					,'bookings.dp'
	    					,'bookings.durasi'
	    					,'bookings.jam_mulai'
	    					,'bookings.jam_akhir'
	    					,'bookings.pembooking'
	    					,'bookings.catatan'
	    					,'bookings.lunas'
	    					,'bookings.tanggal_lunas'
	    					,'bookings.total'
	    					,'bookings.jam_booking'
	    					,'lapangans.nama as lapangans'
	    					,'cms_users.name as operator'
	    					,'pakets.nama as paket'
	    					,'bookings.tanggal'
	    				)
	    				;

	    	$data1=$data1->whereNull('bookings.deleted_at')
	    				->select(
	    					'bookings.nomor'
	    					,'pembayarans.jumlah as dp'
	    					,DB::raw('CONCAT(" "," ") as jam_mulai')
	    					,DB::raw('CONCAT(" "," ") as jam_akhir')
	    					,DB::raw('CONCAT(" "," ") as durasi')
	    					,DB::raw('CONCAT(" "," ") as pembooking')
	    					,DB::raw('CONCAT(" "," ") as catatan')
	    					,DB::raw('CONCAT(" "," ") as lunas')
	    					,DB::raw('CONCAT(" "," ") as tanggal_lunas')
	    					,DB::raw('CONCAT(" "," ") as total')
	    					,DB::raw('CONCAT(" "," ") as jam_booking')
	    					,DB::raw('CONCAT(" "," ") as lapangans')
	    					,DB::raw('CONCAT(" "," ") as operator')
	    					,DB::raw('CONCAT(" "," ") as paket')
	    					,'pembayarans.tanggal'
	    				)
	    				->union($hasil)
	    				->get()
	    				->toArray()
	    				;
			// dd($data1);

	    	return response()->json(['data1'=>$data1,'data2'=>$data2]);
	    }

	    public function getprint($id){
	    	$book = DB::table('bookings')
				    	->leftJoin('lapangans','lapangans.id','=','bookings.lapangans_id')
				    	->leftJoin('pakets','pakets.id','=','bookings.pakets_id')
				    	->where('bookings.id',$id)
				    	->select(
				    		'bookings.nomor',
				    		'bookings.tanggal',
				    		'bookings.durasi',
				    		'bookings.total',
				    		'bookings.lunas',
				    		'bookings.dp',
				    		'lapangans.nama as lapangan',
				    		'pakets.nama as pakets',
				    		'pakets.jam as pakets_jam',
				    		'pakets.harga as pakets_harga'
				    	)
				    	->first();
	    	return view('custom.print',['b'=>$book
	    								,
	    							]);
	    }
	}